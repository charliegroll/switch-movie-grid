var express = require('express'),
    routes = require('./routes'),
    config = require('./config'),
    http = require('http'),
    path = require('path'),
    request = require('request');

var app = express(),
    nyt_options = config.nyt_options;

app.configure(function(){
    app.set('port', config.port || 3000);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
    app.use(express.errorHandler());
});

var getMovies = function (req, res, next) {
    var page,
        offset,
        nyt_callback;

    res.header( 'Access-Control-Allow-Origin', '*' );
    res.header( 'Access-Control-Allow-Method', 'POST, GET, PUT, DELETE, OPTIONS' );
    res.header( 'Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-File-Name, Content-Type, Cache-Control' );

    if( 'OPTIONS' == req.method ) {
        res.send( 203, 'OK' );
    }

    console.log(req.query.page);

    console.log("REST :: getMovies");

    page = parseInt(req.query.page) || 1;
    if (page <= 0) res.send(500, "Page must be an integer, 1 or greater");

    offset = (page-1) * 20;
    nyt_options.offset = offset;

    request({url:config.nyt.url, qs: nyt_options}, function (err, response, body) {
        var json;
        if (err) res.send(500);
        res.send(response.statusCode, body);
    });
}

app.use('/*',function(req,res,next){
    res.header('Vary', 'Accept');
    next();
});

app.get('/', routes.index);
app.get('/movies', getMovies);
app.get('/:page', routes.pages);

http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});
