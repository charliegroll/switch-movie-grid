
/*
 * GET home page.
 */
var request = require('request');

var config = require('../config');

var getMovies = function (res, page) {
    var nyt_options = config.nyt_options;
    page = page || 1;
    nyt_options.offset = (page-1) * 20;

    request({url:config.nyt.url, qs: nyt_options}, function (err, response, body) {
        res.render('index', { title: config.title, json: body, page: page});
    });
}

exports.index = function (req, res) {
    nyt_options = config.nyt_options;
    getMovies(res);
}

exports.pages = function (req, res) {
    var page = req.params.page;
    getMovies(res, page);
}