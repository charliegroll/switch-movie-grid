# Switch Movie Grid

This is my submission for the [Switch Movie Grid][1]

- Pulls from the NYT Movie Reviews API (with pagination)
- Pushes browser history for ajax pagination (tested on Chrome)
- Exposes a simpler RESTful API for the NYT API (http://localhost:3000/movies with optional page parameter) 

### Tech

* [Twitter Bootstrap] - UI boilerplate
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [[Request]] - simplified HTTP request client
* [[SpinKit]] - CSS3 spinner [@tobiasahlin]
* [jQuery]

### Installation
Open your favorite Terminal, clone this repo, and run these commands.

First Tab:
```sh
$ cd switch-movie-grid
$ wget [insert url for raw config.js, provided separately]
$ npm install
$ npm start
```

And visit http://localhost:3000

[1]:https://docs.google.com/document/d/1Q0Vk39tnzMQH4EFohvYgNi6lR0sQTJG-UNTG2Ug3UEU/edit#
[2]:https://gist.github.com/charliegroll/2d7bd5dc562ae27c6a5f
[request]:https://github.com/request/request
[spinkit]:https://github.com/tobiasahlin/SpinKit
