var first_load = true,
    popped = false,
    num_results,
    current_results;

$(document).ready(function () {
    var $movie_container = $('#movie-container'),
        $prev = $('#movie-container button.prev'),
        $next = $('#movie-container button.next'),
        default_img = 'http://dummyimage.com/75/eee/ff0000&text=no+image',
        blank_img = 'http://dummyimage.com/75/fff/fff';

    num_results = $movie_container.data('num-results');

    $('[data-toggle="tooltip"]').tooltip();

    var getMovies = function (page) {
        $.ajax({
            url: '/movies',
            data: {
                page: page
            }
        }).done(function (data) {
            current_results = JSON.parse(data).results;

            if (first_load)
                return;

            $('#movies .movie-item').each(function (i) {
                var result = current_results[i],
                    title = result.display_title,
                    imgurl = default_img,
                    img = $(this).find('img');
                if (result) {
                    if (result.multimedia 
                        && result.multimedia.resource && result.multimedia.resource.src)
                            imgurl = result.multimedia.resource.src;
                } else {
                    imgurl = blank_img;
                }

                img.attr('data-original-title', title);
                img[0].src = imgurl;
            });

            if (!popped) {
                setPage(page);
            }
            $prev.prop('disabled', false);
            $next.prop('disabled', false);
            if (page == 1) $prev.prop('disabled', true);
            else if (page == Math.ceil(num_results/20)) $next.prop('disabled', true);
        });
    }

    var getPage = function () {
        return parseInt($movie_container.attr('data-page'));
    }

    var setPage = function (page) {
        $movie_container.attr('data-page', page);
        if (page == 1) page = '/';
        console.log('set: '+ page)
        window.history.pushState({page: page}, null, page);
    }

    var incPage = function () {
        var page = getPage() + 1;

        getMovies(page);
    }

    var decPage = function () {
        var page = getPage() - 1;

        getMovies(page);
    }
    getMovies(getPage());

    $(document).bind('ajaxSend', function() {
        $('.spinner').show();
        $movie_container.addClass('loading');
        $next.prop('disabled', true);
        $prev.prop('disabled', true);
    }).bind('ajaxStop', function() {
        $('.spinner').hide();
        $movie_container.removeClass('loading');
        
        if (!first_load) {
            $next.prop('disabled', false);
            $prev.prop('disabled', false);
        } else {
            first_load = false;
        }
    }).bind('ajaxError', function() {
        $('.spinner').hide();
        $movie_container.removeClass('loading');
        $next.prop('disabled', false);
        $prev.prop('disabled', false);
    });

    $prev.click(decPage);
    $next.click(incPage);

    $('#movies .movie-item').hover(function () {
        $(this).find('img').toggleClass('hover-pointer');
    })

    $('#movies .movie-item').click(function () {
        var index = $(this).data('index'),
            result = current_results[index];

        $('.modal-title').text(result.display_title);
        $('.summary').html(result.summary_short);
        $('.opening-date span').text(result.opening_date);
        $('.rating span').text(result.mpaa_rating || 'Unrated');
        if (result.critics_pick == 1) {
            $('.critics-pick span').removeClass().addClass('glyphicon glyphicon-ok');
        } else {
            $('.critics-pick span').removeClass().addClass('glyphicon glyphicon-remove');
        }
        if (result.link) {
            $('.link a').text(result.link.suggested_link_text).attr('href', result.link.url);
        } else {
            $('.link a').text('').attr('href', '');
        }
        if (result.related_urls) {
            for (var i = 0; i < result.related_urls.length; i++) {
                if (result.related_urls[i].type == 'overview') {
                    $('.modal-header a').attr('href', result.related_urls[i].url);
                }
            }
        }
    });

    $('.modal').on('shown.bs.modal', function () {
        $('.tooltip').prev().tooltip('hide');
    });  

    $(window).on("popstate", function () {
        if (history.state) {
            popped = true;
            getMovies(history.state.page);
        }
    });
});
